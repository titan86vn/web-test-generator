﻿using System;
using System.Xml.Linq;
using System.IO;
using System.Windows.Forms;

namespace WebLoadTestHelper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLoadFromFile_Click(object sender, EventArgs e)
        {
            DialogResult result = dlgSelectFile.ShowDialog();

            if (result == DialogResult.OK)
            {
                string filePath = dlgSelectFile.FileName;

                try
                {
                    string[] lines = File.ReadAllLines(filePath);
                    txtSource.Text = string.Join("\r\n", lines);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format(@"Cannot read from file {0} due {1}", dlgSelectFile.FileName, ex.Message));
                }
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            var xDoc = new XDocument(new XDeclaration("1.0", "utf-8", "true"));
            var xWebTest = new XElement("WebTest", 
                new XAttribute("Name", "Generator_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")),
                new XAttribute("Id", Guid.NewGuid()),
                new XAttribute("Owner", string.Empty),
                new XAttribute("Priority", new Random().Next(10000, 100000)),
                new XAttribute("Enabled", true),
                new XAttribute("CssProjectStructure", string.Empty),
                new XAttribute("CssIteration", string.Empty),
                new XAttribute("Timeout", 0),
                new XAttribute("WorkItemIds", string.Empty),
                new XAttribute("Description", string.Empty),
                new XAttribute("CredentialUserName", string.Empty),
                new XAttribute("CredentialPassword", string.Empty),
                new XAttribute("PreAuthenticate", true),
                new XAttribute("Proxy", "default"),
                new XAttribute("StopOnError", false),
                new XAttribute("RecordedResultFile", string.Empty),
                new XAttribute("ResultsLocale", string.Empty));
            var xItem = new XElement("Items");

            foreach (var line in txtSource.Lines)
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }

                var xRequest = new XElement("Request",
                    new XAttribute("Method", "GET"),
                    new XAttribute("Guid", Guid.NewGuid()),
                    new XAttribute("Version", "1.1"),
                    new XAttribute("Url", Uri.EscapeUriString(line)),
                    new XAttribute("ThinkTime", 5),
                    new XAttribute("Timeout", 300),
                    new XAttribute("ParseDependentRequests", false),
                    new XAttribute("FollowRedirects", false),
                    new XAttribute("RecordResult", true),
                    new XAttribute("Cache", false),
                    new XAttribute("ResponseTimeGoal", 5),
                    new XAttribute("Encoding", "utf-8"),
                    new XAttribute("ExpectedHttpStatusCode", 0),
                    new XAttribute("ExpectedResponseUrl", string.Empty),
                    new XAttribute("ReportingName", string.Empty),
                    new XAttribute("IgnoreHttpStatusCode", false));

                xItem.Add(xRequest);
            }

            xWebTest.Add(xItem);
            xDoc.Add(xWebTest);

            txtTarget.Text = xDoc.ToString();
        }
    }
}
